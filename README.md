![alt text](https://app.bitrise.io/app/5515a5a80c58b6ec/status.svg?token=uuUlHBWbhZcBi6xXfGcB9g&branch=master "Bitrise build status")

## GithubFeed
 Simple Android app (100% Kotlin) that fetches list of repos for a given Github user name.
 It displays the repo name, user avatar and the stars count for the given Github user.
 Clicking the repo name opens an external browser with the website view of that repo.
 
 Some Unit tests added. Simple error handling.
 
