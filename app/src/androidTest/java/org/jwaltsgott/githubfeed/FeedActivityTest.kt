package org.jwaltsgott.githubfeed

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.jwaltsgott.githubfeed.ui.FeedActivity
import org.koin.test.AutoCloseKoinTest

@RunWith(AndroidJUnit4::class)
class FeedActivityTest : AutoCloseKoinTest() {

    @get:Rule
    val rule = ActivityTestRule(FeedActivity::class.java)

    @Test
    fun editTextShouldBeVisible() {
        onView(withId(R.id.et_user_name)).check(matches(isDisplayed()))
    }
}
