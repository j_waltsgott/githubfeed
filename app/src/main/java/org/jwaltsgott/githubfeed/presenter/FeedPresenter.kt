package org.jwaltsgott.githubfeed.presenter

import io.reactivex.disposables.CompositeDisposable
import org.jwaltsgott.githubfeed.BuildConfig
import org.jwaltsgott.githubfeed.model.Repo
import org.jwaltsgott.githubfeed.model.User
import org.jwaltsgott.githubfeed.model.network.GithubApi
import org.jwaltsgott.githubfeed.support.Schedulers

class FeedPresenter(
    val schedulers: Schedulers,
    private val githubApi: GithubApi
) : FeedContract.Presenter {

    private var view: FeedContract.View? = null
    private var request: CompositeDisposable = CompositeDisposable()

    override fun takeView(view: FeedContract.View) {
        this.view = view
    }

    override fun stop() {
        view = null
        request.clear()
    }

    override fun query(userName: String) {
        view?.showRepos(emptyList())
        view?.resetUserInfo()
        view?.showProgress()

        request.clear()

        request.add(githubApi.queryRepos(userName)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.mainThread())
            .subscribe(
                { repos -> handleResponse(repos) },
                { error -> handleError(error) }
            )
        )

        request.add(githubApi.queryUser(userName)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.mainThread())
            .subscribe(
                { user -> handleUser(user) },
                { _ -> Unit }
            )
        )
    }

    override fun onRepoClicked(repo: Repo) {
        view?.goToUrl(repo.html_url)
    }

    private fun handleResponse(repos: List<Repo>) {
        view?.hideProgress()

        if (repos.isEmpty()) {
            view?.showNoRepos()
        } else {
            view?.showRepos(repos)
        }
    }

    private fun handleError(error: Throwable) {
        if (BuildConfig.DEBUG) error.printStackTrace()

        view?.hideProgress()
        view?.showError()
    }

    private fun handleUser(user: User) {
        view?.showUserInfo(user)
    }
}