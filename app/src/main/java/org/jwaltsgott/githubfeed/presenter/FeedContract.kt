package org.jwaltsgott.githubfeed.presenter

import org.jwaltsgott.githubfeed.model.Repo
import org.jwaltsgott.githubfeed.model.User

interface FeedContract {

    interface Presenter {
        fun takeView(view: View)
        fun stop()
        fun query(userName: String)
        fun onRepoClicked(repo: Repo)
    }

    interface View {
        fun showProgress()
        fun hideProgress()
        fun showError()
        fun showRepos(repos: List<Repo>)
        fun showNoRepos()
        fun goToUrl(url: String)
        fun showUserInfo(user: User)
        fun resetUserInfo()
    }
}