package org.jwaltsgott.githubfeed

import org.jwaltsgott.githubfeed.model.network.BASE_URL
import org.jwaltsgott.githubfeed.model.network.ConverterProvider
import org.jwaltsgott.githubfeed.model.network.GithubApi
import org.jwaltsgott.githubfeed.model.network.OkHttpClientProvider
import org.jwaltsgott.githubfeed.model.network.RetrofitApiService
import org.jwaltsgott.githubfeed.model.network.RetrofitProvider
import org.jwaltsgott.githubfeed.presenter.FeedContract
import org.jwaltsgott.githubfeed.presenter.FeedPresenter
import org.jwaltsgott.githubfeed.support.Schedulers
import org.jwaltsgott.githubfeed.support.SchedulersImpl
import org.koin.dsl.module.module
import retrofit2.Converter

val applicationModules = listOf(
    module {
        // Singletons
        single { OkHttpClientProvider().okHttpClient }
        single<Converter.Factory> { ConverterProvider().gsonConverterFactory }
        single { RetrofitProvider(get(), get(), BASE_URL).retrofit }
        single<Schedulers> { SchedulersImpl() }
        single<GithubApi> { RetrofitApiService(get()) }

        // Factories
        factory<FeedContract.Presenter> { FeedPresenter(get(), get()) }
    }
)