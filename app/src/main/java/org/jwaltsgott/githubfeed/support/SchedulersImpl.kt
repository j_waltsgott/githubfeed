package org.jwaltsgott.githubfeed.support

import io.reactivex.android.schedulers.AndroidSchedulers

class SchedulersImpl : Schedulers {
    override fun io() = io.reactivex.schedulers.Schedulers.io()

    override fun computation() = io.reactivex.schedulers.Schedulers.computation()

    override fun mainThread() = AndroidSchedulers.mainThread()
}