package org.jwaltsgott.githubfeed.model.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jwaltsgott.githubfeed.BuildConfig

class OkHttpClientProvider(
    val okHttpClient: OkHttpClient = OkHttpClient.Builder().apply {
        if (BuildConfig.DEBUG) {
            addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
        }
    }.build()
)