package org.jwaltsgott.githubfeed.model.network

import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

class RetrofitProvider(
    okHttpClient: OkHttpClient,
    converterFactory: Converter.Factory,
    baseUrl: String
) {
    val retrofit: Retrofit =
        Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(converterFactory)
            .build()
}