package org.jwaltsgott.githubfeed.model.network

import retrofit2.converter.gson.GsonConverterFactory

class ConverterProvider {
    val gsonConverterFactory by lazy { GsonConverterFactory.create() }
}