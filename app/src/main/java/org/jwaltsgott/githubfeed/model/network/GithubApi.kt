package org.jwaltsgott.githubfeed.model.network

import io.reactivex.Single
import org.jwaltsgott.githubfeed.model.Repo
import org.jwaltsgott.githubfeed.model.User
import retrofit2.http.GET
import retrofit2.http.Path

const val BASE_URL: String = "https://api.github.com/"

interface GithubApi {

    @GET("users/{userName}/repos")
    fun queryRepos(@Path("userName") userName: String): Single<List<Repo>>

    @GET("users/{userName}")
    fun queryUser(@Path("userName") userName: String): Single<User>
}