package org.jwaltsgott.githubfeed.model

data class Repo(val name: String, val stargazers_count: Int, val html_url: String)