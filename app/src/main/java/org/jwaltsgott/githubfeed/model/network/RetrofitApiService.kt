package org.jwaltsgott.githubfeed.model.network

import retrofit2.Retrofit

class RetrofitApiService(
    retrofit: Retrofit,
    val gitHubApi: GithubApi = retrofit.create(GithubApi::class.java)
) : GithubApi by gitHubApi