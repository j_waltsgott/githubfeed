package org.jwaltsgott.githubfeed.model

data class User(val name: String, val avatar_url: String)