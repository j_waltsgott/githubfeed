package org.jwaltsgott.githubfeed.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.jwaltsgott.githubfeed.R
import org.jwaltsgott.githubfeed.model.Repo

class RepoAdapter(
    private var repos: List<Repo>,
    private val listener: RepoAdapterListener
) : RecyclerView.Adapter<RepoAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.text_name)
        val stars = itemView.findViewById<TextView>(R.id.text_star_count)
    }

    interface RepoAdapterListener {
        fun onRepoClicked(repo: Repo)
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RepoAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_item_feed_repo, parent, false) as RelativeLayout

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name?.text = repos.get(position).name
        holder.stars?.text = repos.get(position).stargazers_count.toString()
        holder.name?.setOnClickListener { listener.onRepoClicked(repos.get(position)) }
    }

    override fun getItemCount() = repos.size

    fun setRepos(repos: List<Repo>) {
        this.repos = repos
        notifyDataSetChanged()
    }
}
