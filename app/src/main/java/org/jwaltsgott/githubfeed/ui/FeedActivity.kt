package org.jwaltsgott.githubfeed.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_feed.*
import org.jwaltsgott.githubfeed.R
import org.jwaltsgott.githubfeed.model.Repo
import org.jwaltsgott.githubfeed.model.User
import org.jwaltsgott.githubfeed.presenter.FeedContract
import org.koin.android.ext.android.inject

class FeedActivity : AppCompatActivity(), FeedContract.View, RepoAdapter.RepoAdapterListener {

    private val presenter: FeedContract.Presenter by inject()

    private val imm: InputMethodManager by lazy {
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    private lateinit var repoAdapter: RepoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed)

        et_user_name.setOnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_SEARCH -> doSearch()
                else -> false
            }
        }

        repoAdapter = RepoAdapter(emptyList(), this)
        recylerView.apply {
            layoutManager = LinearLayoutManager(this@FeedActivity)
            adapter = repoAdapter
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.takeView(this)
    }

    override fun onStop() {
        presenter.stop()
        super.onStop()
    }

    override fun showProgress() {
        layout_loading.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        layout_loading.visibility = View.GONE
    }

    override fun showError() {
        Snackbar.make(
            constraint_layout,
            getString(R.string.feed_error),
            Snackbar.LENGTH_LONG
        )
            .show()
    }

    override fun showRepos(repos: List<Repo>) {
        Log.d("FeedActivity", "Got ${repos.size} repos")
        repoAdapter.setRepos(repos)
    }

    override fun showNoRepos() {
        // show keyboard to retry search
        imm.showSoftInput(et_user_name, InputMethodManager.SHOW_IMPLICIT)

        Snackbar.make(
            constraint_layout,
            getString(R.string.feed_msg_no_repos_found),
            Snackbar.LENGTH_LONG
        )
            .show()
    }

    override fun goToUrl(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    override fun onRepoClicked(repo: Repo) {
        presenter.onRepoClicked(repo)
    }

    override fun showUserInfo(user: User) {
        Picasso.get()
            .load(Uri.parse(user.avatar_url))
            .placeholder(R.drawable.ic_person_accent_24dp)
            .error(R.drawable.ic_person_accent_24dp)
            .into(iv_user)
    }

    override fun resetUserInfo() {
        Picasso.get()
            .load(R.drawable.ic_person_accent_24dp)
            .into(iv_user)
    }

    fun doSearch(): Boolean {
        presenter.query(et_user_name.text.toString().trim())

        // hide keyboard while searching
        imm.hideSoftInputFromWindow(et_user_name.windowToken, 0)

        return true
    }
}
