package org.jwaltsgott.githubfeed

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.jwaltsgott.githubfeed.model.Repo
import org.jwaltsgott.githubfeed.model.User
import org.jwaltsgott.githubfeed.model.network.GithubApi
import org.jwaltsgott.githubfeed.presenter.FeedContract
import org.jwaltsgott.githubfeed.presenter.FeedPresenter

private const val QUERY1: String = "test"

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TestFeedPresenter {

    private val view: FeedContract.View = mock()
    private val api: GithubApi = mock()
    private val schedulers: org.jwaltsgott.githubfeed.support.Schedulers = mock()

    private lateinit var feedPresenter: FeedContract.Presenter

    @Before
    fun setUp() {
        feedPresenter = FeedPresenter(schedulers, api)

        whenever(schedulers.io()).thenReturn(Schedulers.trampoline())
        whenever(schedulers.mainThread()).thenReturn(Schedulers.trampoline())

        whenever(api.queryUser(QUERY1)).thenReturn(Single.just(User("name", "url")))
    }

    @Test
    fun testPresenterWithEmptyResult() {
        whenever(api.queryRepos(QUERY1)).thenReturn(Single.just(emptyList()))

        feedPresenter.takeView(view)
        feedPresenter.query(QUERY1)

        verify(view).resetUserInfo()
        verify(view).showProgress()
        verify(view).hideProgress()
        verify(view).showNoRepos()

        feedPresenter.stop()
    }

    @Test
    fun testPresenterWithNonEmptyResult() {
        val list: List<Repo> = listOf(
                Repo("name", 1, "bla"),
                Repo("name2", 2, "bla")
        )

        whenever(api.queryRepos(QUERY1)).thenReturn(Single.just(list))

        feedPresenter.takeView(view)
        feedPresenter.query(QUERY1)

        verify(view).resetUserInfo()
        verify(view).showProgress()
        verify(view).hideProgress()
        verify(view).showRepos(list)

        feedPresenter.stop()
    }

    @Test
    fun testPresenterWithRepoClicked() {
        val url = "myUrl"
        val repo = Repo("name", 1, url)

        feedPresenter.takeView(view)
        feedPresenter.onRepoClicked(repo)

        verify(view).goToUrl(url)

        feedPresenter.stop()
    }

    @Test
    fun testPresenterWithErrorResult() {
        whenever(api.queryRepos(QUERY1)).thenReturn(Single.error(Throwable()))

        feedPresenter.takeView(view)
        feedPresenter.query(QUERY1)

        verify(view).resetUserInfo()
        verify(view).showProgress()
        verify(view).hideProgress()
        verify(view).showError()

        feedPresenter.stop()
    }

    @Test
    fun testPresenterWithUserInfo() {
        val user = User("userName", "avatarUrl")

        whenever(api.queryUser(QUERY1)).thenReturn(Single.just(user))
        whenever(api.queryRepos(QUERY1)).thenReturn(Single.just(emptyList()))

        feedPresenter.takeView(view)
        feedPresenter.query(QUERY1)

        verify(view).resetUserInfo()
        verify(view).showProgress()
        verify(view).hideProgress()
        verify(view).showUserInfo(user)

        feedPresenter.stop()
    }

}
